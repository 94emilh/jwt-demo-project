package com.example.jwtdemoproject.mock;

import com.example.jwtdemoproject.auth.RegisterRequest;
import com.example.jwtdemoproject.model.entity.Role;

public final class RegisterRequestMock {

    private RegisterRequestMock(){
        //empty
    }

    public static RegisterRequest getRegisterRequestWithAdminRole() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setFirstname("Emil");
        registerRequest.setLastname("H");
        registerRequest.setEmail("emilh@gmail.com");
        registerRequest.setPassword("password123");
        registerRequest.setRole(Role.ADMIN);
        return registerRequest;
    }

    public static RegisterRequest getRegisterRequestWithManagerRole() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setFirstname("Vasile");
        registerRequest.setLastname("P");
        registerRequest.setEmail("vasilep@gmail.com");
        registerRequest.setPassword("password123");
        registerRequest.setRole(Role.MANAGER);
        return registerRequest;
    }
}
