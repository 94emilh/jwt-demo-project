package com.example.jwtdemoproject.mock;

import com.example.jwtdemoproject.auth.AuthenticationRequest;

public final class AuthenticationRequestMock {

    private AuthenticationRequestMock() {
        //empty
    }

    public static AuthenticationRequest getAuthenticationRequestForAdminRole(){
        AuthenticationRequest authenticationRequest = new AuthenticationRequest();
        authenticationRequest.setEmail("emilh@gmail.com");
        authenticationRequest.setPassword("password123");
        return authenticationRequest;
    }

    public static AuthenticationRequest getAuthenticationRequestForManagerRole(){
        AuthenticationRequest authenticationRequest = new AuthenticationRequest();
        authenticationRequest.setEmail("vasilep@gmail.com");
        authenticationRequest.setPassword("password123");
        return authenticationRequest;
    }
}
