package com.example.jwtdemoproject.integration;

import com.example.jwtdemoproject.JwtDemoProjectApplication;
import com.example.jwtdemoproject.auth.AuthenticationRequest;
import com.example.jwtdemoproject.auth.AuthenticationResponse;
import com.example.jwtdemoproject.auth.RegisterRequest;
import com.example.jwtdemoproject.mock.AuthenticationRequestMock;
import com.example.jwtdemoproject.repository.TokenRepository;
import com.example.jwtdemoproject.repository.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.stream.Stream;

import static com.example.jwtdemoproject.mock.RegisterRequestMock.getRegisterRequestWithAdminRole;
import static com.example.jwtdemoproject.mock.RegisterRequestMock.getRegisterRequestWithManagerRole;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

@ActiveProfiles("integration-test")
@AutoConfigureMockMvc
@ComponentScan(basePackageClasses = JwtDemoProjectApplication.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = JwtDemoProjectApplication.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource({"classpath:application-integration.properties"})
class JwtDemoProjectApplicationIT {

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ObjectMapper objectMapper;

    private static Headers headers;

    @LocalServerPort
    private int port;

    @BeforeAll
    void init() {
        tokenRepository.deleteAll();
        userRepository.deleteAll();
    }

    @AfterEach
    public void afterEach() {
        userRepository.deleteAll();
        userRepository.flush();
        tokenRepository.deleteAll();
        tokenRepository.flush();
    }

    private static Stream<Boolean> logOutAtTheEndScenario() {
        return Stream.of(false, true);
    }

    @ParameterizedTest
    @MethodSource("logOutAtTheEndScenario")
    void whenRegisteringAndUsingTokenFromResponseThen200IsReturnedAndAlsoAfterLogout403IsReturned(boolean isItLogOutAtTheEndScenario) throws JsonProcessingException {
        RegisterRequest registerRequest = getRegisterRequestWithAdminRole();

        String registerResponseAsString = given().contentType(ContentType.JSON).accept(ContentType.JSON)
                                                 .body(registerRequest)
                                                 .when()
                                                 .port(port)
                                                 .post("/api/v1/auth/register")
                                                 .then()
                                                 .assertThat().statusCode(is(200))
                                                 .extract().response().getBody().asString();


        extractTokenFromResponseAndAddItToHeaders(registerResponseAsString);

        //without token on headers - 403 status code
        given().contentType(ContentType.JSON).accept(ContentType.JSON)
               .when()
               .port(port).get("/helloWorld/user")
               .then()
               .assertThat().statusCode(is(403));

        //with token on headers - 200 status code
        given().contentType(ContentType.JSON).accept(ContentType.JSON)
               .headers(headers)
               .when()
               .port(port).get("/helloWorld/user")
               .then()
               .assertThat().statusCode(is(200));

        if (isItLogOutAtTheEndScenario) {
            //logging out
            given().contentType(ContentType.JSON).accept(ContentType.JSON)
                   .headers(headers)
                   .when()
                   .port(port)
                   .post("/api/v1/auth/logout")
                   .then()
                   .assertThat().statusCode(is(200))
                   .extract().response().getBody().asString();

            //trying to access existing resource with above used token does not work anymore
            given().contentType(ContentType.JSON).accept(ContentType.JSON)
                   .headers(headers)
                   .when()
                   .port(port).get("/helloWorld/user")
                   .then()
                   .assertThat().statusCode(is(403));
        }
    }

    @Test
    void whenRegisteringAuthenticatingIsSuccessfulWithGivenCredentialsUsingTokenFromResponseThen200IsReturnedForExistingEndpoint() throws JsonProcessingException {
        RegisterRequest registerRequest = getRegisterRequestWithManagerRole();

        given().contentType(ContentType.JSON).accept(ContentType.JSON)
               .body(registerRequest)
               .when()
               .port(port)
               .post("/api/v1/auth/register")
               .then()
               .assertThat().statusCode(is(200));

        AuthenticationRequest authenticationRequest = AuthenticationRequestMock.getAuthenticationRequestForManagerRole();

        String authenticationResponseAsString = given().contentType(ContentType.JSON).accept(ContentType.JSON)
                                                       .body(authenticationRequest)
                                                       .when()
                                                       .port(port)
                                                       .post("/api/v1/auth/authenticate")
                                                       .then()
                                                       .assertThat().statusCode(is(200))
                                                       .extract().response().getBody().asString();

        extractTokenFromResponseAndAddItToHeaders(authenticationResponseAsString);

        //without token on headers - 403 status code
        given().contentType(ContentType.JSON).accept(ContentType.JSON)
               .when()
               .port(port).get("/helloWorld/user")
               .then()
               .assertThat().statusCode(is(403));

        //with token on headers - 200 status code
        given().contentType(ContentType.JSON).accept(ContentType.JSON)
               .headers(headers)
               .when()
               .port(port).get("/helloWorld/user")
               .then()
               .assertThat().statusCode(is(200));
    }

    @Test
    void whenAuthenticatingWithNotExistingAccount403IsReturned() {
        RegisterRequest registerRequest = getRegisterRequestWithManagerRole();

        given().contentType(ContentType.JSON).accept(ContentType.JSON)
               .body(registerRequest)
               .when()
               .port(port)
               .post("/api/v1/auth/register")
               .then()
               .assertThat().statusCode(is(200));

        AuthenticationRequest authenticationRequest = AuthenticationRequestMock.getAuthenticationRequestForAdminRole();

        given().contentType(ContentType.JSON).accept(ContentType.JSON)
               .body(authenticationRequest)
               .when()
               .port(port)
               .post("/api/v1/auth/authenticate")
               .then()
               .assertThat().statusCode(is(403))
               .extract().response().getBody().asString();
    }

    private void extractTokenFromResponseAndAddItToHeaders(String registerResponseAsString) throws JsonProcessingException {
        AuthenticationResponse responseObject = objectMapper.readValue(registerResponseAsString, AuthenticationResponse.class);
        String generatedAccessToken = responseObject.getAccessToken();
        headers = putTokenOnHeadersAndReturnIt(generatedAccessToken);
    }

    private Headers putTokenOnHeadersAndReturnIt(String token) {
        Header authorization = new Header("Authorization", "Bearer " + token);
        return new Headers(authorization);
    }

}
